import React from 'react';
import Assessment from 'material-ui/svg-icons/action/assessment';

const data = {
  menus: [
    { text: 'DashBoard', icon: <Assessment/>, link: '/' },
  ],
};

export default data;

import React from 'react';
import globalStyles from '../styles';
import LineChart from '../components/dashboard/LineChart';

const DashboardPage = () => {

  return (
    <div>
        <h3 style={globalStyles.navigation}>Application / Dashboard</h3>

        <div className="row">

        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-12 m-b-15 ">
            <LineChart/>
        </div> 
        </div>
    </div>
       
  );
};

export default DashboardPage;
import React, { Component } from 'react';
import { render } from 'react-dom';
import Highcharts from 'highcharts';
import ReactHighcharts from 'react-highcharts';
import socketIOClient from "socket.io-client";


//====================================== Variable Declaration =====================================

let displayValues = [];


function insertDatapoints(message) {
  let ecgData
  for (let i = 0; i < message.length; i++) {
    
    ecgData = {
    timestamp: message[i].timestamp,
    ecg: message[i].ecg
  };


  return ecgData;
}
}
export default class LineChart extends Component {

  constructor() {
    super();
    this.state = {
        ecg: false,
        endpoint: "http://localhost:5000/"
    };
  }
  

  componentDidMount() {

    let ecgData;

    const {endpoint} = this.state;
    //Very simply connect to the socket
    const socket = socketIOClient(endpoint);
    //Listen for data on the "outgoing data" namespace and supply a callback for what to do when we get one. In this case, we set a state variable
    
    //====================================== Socket Connection======================================

    socket.on('connect', () => {
      console.log('Connected');
    });

    socket.on('disconnect', () => {
      console.log('Disconnected');
    });

    socket.on("ecgData", message => {

      displayValues = insertDatapoints(message);
     
    });
    
    
    let chart = this.refs.chart.getChart();
    //chart.series[0].addPoint({x: 10, y: 12});

    // set up the updating of the chart each second
    var series = chart.series[0];
    setInterval(function () {
      var x = (new Date()).getTime(), // current time
        y =   displayValues.ecg;

        

      series.addPoint([x, y], true, true);
  
    }, 1000);

    

  }
  static formatTooltip(tooltip, x = this.x, y = this.y, series = this.series) {
    return `<b>${x}</b><br/>${series.name}: ${y}`;
  }

  static getConfig = (navs) => ({
    chart: {
      type: 'spline',
      animation: Highcharts.svg, // don't animate in old IE
      marginRight: 10,
    },
    title: {
      text: 'ECG Data'
    },
    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Value'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }]
    },
    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + '</b><br/>' +
          Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
          Highcharts.numberFormat(this.y, 2);
      }
    },
    legend: {
      enabled: false
    },
    exporting: {
      enabled: false
    },
    series: [{
      name: 'ECG',
      data: (function () {
        // generate an array of random data
        var data = [],
          time = (new Date()).getTime(),
          i;

        for (i = -19; i <= 0; i += 1) {
          data.push({
            x: (new Date()).getTime(),
            y: displayValues.ecg
            
          });
        }
       
        return data;
      }())
    }]
  })

  render() {
    return (
      <div>
        <ReactHighcharts config={LineChart.getConfig(this.props.navs)} ref="chart" />
      </div>
    );
  }
}
